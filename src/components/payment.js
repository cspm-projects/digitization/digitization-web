import React from 'react';
import Cards from 'react-credit-cards';
import {css} from "@emotion/react";
import {BeatLoader} from "react-spinners";
import axios from "axios";
import {v4 as uuidv4} from 'uuid';
import moment from "moment/moment";
import {Button, Modal, Alert} from "react-bootstrap";

const override = css`
  margin: 0 auto;
  border-color: deepskyblue;
`;

export default class PaymentForm extends React.Component {

    checkCardUrl = process.env.REACT_APP_BASE_URL + "/autorizador/verificarElegibilidad";
    reqCardUrl = process.env.REACT_APP_BASE_URL + "/autorizador/solicitarDigitalizacion";
    sendOtpUrl = process.env.REACT_APP_BASE_URL + "/notificacion/enviarOTP";
    notifyUrl = process.env.REACT_APP_BASE_URL + "/notificacion/cambioToken";

    correlationId = uuidv4();
    otp = 100000;
    input = React.createRef();

    responsesInit = {
        checkCard: null,
        requestCard: null,
        notify: null,
        sendOtp: null,
    }

    controlStateInit = {
        showForm: true,
        showOtp: false,
        checkCard: {
            state: 'No iniciado',
            response: null,
            time: null
        },
        reqCard: {
            state: 'No iniciado',
            response: null,
            time: null
        },
        sendOtp: {
            state: 'Inactivo',
            response: null,
            time: null
        },
        notify: {
            state: 'No iniciado',
            response: null,
            time: null
        },
    }

    responseState = this.responsesInit;

    state = {
        cvc: '',
        expiryDate: '',
        focus: '',
        cardHolder: '',
        cardNumber: '',
        document: '100240712',
        wallet: 'GGRN',
        captureMethod: 'APP',
        levelOfTrust: 'AMARILLO',
        securityScore: '1',
        controlState: this.controlStateInit
    };


    handleInputFocus = (e) => {
        this.setState({focus: e.target.name});
    }

    handleInputChange = (e) => {
        const {name, value} = e.target;

        this.setState({[name]: value});
    }

    handleSubmit = async (e) => {
        this.correlationId = uuidv4();
        this.responseState = this.responsesInit;
        const newState = {...this.state};
        newState.controlState = this.controlStateInit;
        newState.controlState.showForm = false;
        this.setState(newState);

        e.preventDefault();
        const checkCardRes = await this.sendCheckCardRequest();
        if (checkCardRes !== null) {
            const requeCardRes = await this.sendRequestCardRequest(checkCardRes);
            if (requeCardRes !== null) {
                const {estado} = requeCardRes;
                if (estado === 'AMARILLO') {
                    const newState = {...this.state};
                    newState.controlState.showOtp = true;
                    this.setState(newState);
                    const responseOtp = await this.sendsendOtpRequest();
                    console.log(responseOtp);
                } else if (estado === 'VERDE') {
                    const notifyRes = await this.sendNotifyChangeRequest();
                    console.log(notifyRes);
                }
            }
        }
    }

    // Checkcard
    sendCheckCardRequest = () => {
        const start = moment();
        const headers = this.buildCheckCardReqHeaders();
        const req = this.buildCheckCardReq();

        return axios.post(this.checkCardUrl, req, {headers})
            .then(r => {
                if (r.data.estado === 'ROJO') {
                    this.setErrorReqState(start);
                    return null;
                }

                const newState = {...this.state};
                newState.controlState.checkCard.state = 'Finalizado';
                newState.controlState.checkCard.response = 'OK';
                newState.controlState.checkCard.time = moment().diff(start, 'milliseconds');
                this.setState(newState);

                this.validateCheckcardResponse(req, r.data);
                this.responseState.checkCard = r.data;
                return r.data;
            })
            .catch(e => {
                console.log(e);
                this.setErrorReqState(start);
                return null;
            });
    }

    setErrorReqState = (start) => {
        const newState = {...this.state};
        newState.controlState.checkCard.state = 'Finalizado';
        newState.controlState.checkCard.response = 'ERROR';
        newState.controlState.checkCard.time = moment().diff(start, 'milliseconds');
        this.responseState = this.responsesInit;
        setTimeout(() => {
            newState.controlState.showForm = true;
            this.setState(newState);
        }, 3000);
    }

    buildCheckCardReq = () => {
        const newState = {...this.state};
        newState.controlState.checkCard.state = 'Enviado';
        this.setState(newState);

        const reqCheck = {...this.state};
        delete reqCheck.focus;
        delete reqCheck.securityScore;
        delete reqCheck.cardHolder;
        delete reqCheck.document;
        delete reqCheck.levelOfTrust;
        delete reqCheck.controlState;

        return reqCheck;
    }

    buildCheckCardReqHeaders = () => {
        return {
            "Accept": "*/*",
            "transaccionId": this.correlationId
        };
    }

    validateCheckcardResponse = (req, res) => {
        return true;
    }

    // Request card
    sendRequestCardRequest = () => {
        const start = moment();
        const headers = this.buildRequestCardReqHeaders();
        const req = this.buildRequestCardReq(this.responseState.checkCard);

        return axios.post(this.reqCardUrl, req, {headers})
            .then(r => {
                const newState = {...this.state};
                newState.controlState.reqCard.state = 'Finalizado';
                newState.controlState.reqCard.response = 'OK';
                newState.controlState.reqCard.time = moment().diff(start, 'milliseconds');
                this.setState(newState);

                this.validateRequestcardResponse(req, r.data);
                this.responseState.requestCard = r.data;
                return r.data;
            })
            .catch(e => {
                console.log(e);
                const newState = {...this.state};
                newState.controlState.reqCard.state = 'Finalizado';
                newState.controlState.reqCard.response = 'ERROR';
                newState.controlState.reqCard.time = moment().diff(start, 'milliseconds');
                setTimeout(() => {
                    newState.controlState.showForm = true;
                    this.setState(newState);
                }, 3000);
                this.responseState = this.responsesInit;
                return null;
            });
    }

    buildRequestCardReq = (checkRes) => {
        const newState = {...this.state};
        newState.controlState.reqCard.state = 'Enviado';
        this.setState(newState);

        const {issuerCardId} = checkRes;
        const reqCard = {...this.state};
        delete reqCard.focus;
        delete reqCard.controlState;
        reqCard.issuerCardId = issuerCardId;

        return reqCard;
    }

    buildRequestCardReqHeaders = () => {
        return {
            "Accept": "*/*",
            "transaccionId": this.correlationId
        };
    }

    validateRequestcardResponse = (req, res) => {
        return true;
    }

    // Notify
    sendNotifyChangeRequest = () => {
        const start = moment();
        const headers = this.buildNotifyChangeReqHeaders();
        const req = this.buildNotifyChangeReq(this.responseState.checkCard);

        return axios.post(this.notifyUrl, req, {headers})
            .then(r => {
                const newState = {...this.state};
                newState.controlState.notify.state = 'Finalizado';
                newState.controlState.notify.response = 'OK';
                newState.controlState.notify.time = moment().diff(start, 'milliseconds');
                this.setState(newState);

                this.validateNotifyChangeResponse(req, r.data);
                this.responseState.NotifyChange = r.data;
                return r.data;
            })
            .catch(e => {
                console.log(e);
                const newState = {...this.state};
                newState.controlState.notify.state = 'Finalizado';
                newState.controlState.notify.response = 'ERROR';
                newState.controlState.notify.time = moment().diff(start, 'milliseconds');
                setTimeout(() => {
                    newState.controlState.showForm = true;
                    this.setState(newState);
                }, 3000);
                this.responseState = this.responsesInit;
                return null;
            });
    }

    buildNotifyChangeReq = (reqCardRes) => {
        const newState = {...this.state};
        newState.controlState.notify.state = 'Enviado';
        this.setState(newState);

        const {issuerCardId} = reqCardRes;
        const notifyChange = {};
        notifyChange.issuerCardId = issuerCardId;
        notifyChange.wallet = this.state.wallet;
        notifyChange.token = uuidv4().replaceAll("-", "").substr(2, 14);

        return notifyChange;
    }

    buildNotifyChangeReqHeaders = () => {
        return {
            "Accept": "*/*",
            "transaccionId": this.correlationId
        };
    }

    validateNotifyChangeResponse = (req, res) => {
        return true;
    }

    // Otp
    sendsendOtpRequest = () => {
        const start = moment();
        const headers = this.buildsendOtpReqHeaders();
        const req = this.buildsendOtpReq(this.responseState.checkCard);

        return axios.post(this.sendOtpUrl, req, {headers})
            .then(r => {
                const newState = {...this.state};
                newState.controlState.sendOtp.state = 'Finalizado';
                newState.controlState.sendOtp.response = 'OK';
                newState.controlState.sendOtp.time = moment().diff(start, 'milliseconds');
                this.setState(newState);

                this.validatesendOtpResponse(req, r.data);
                this.responseState.sendOtp = r.data;
                return r.data;
            })
            .catch(e => {
                console.log(e);
                this.buildOtpError(start);
                return null;
            });
    }

    buildOtpError = (start) => {
        const newState = {...this.state};
        newState.controlState.sendOtp.state = 'Finalizado';
        newState.controlState.sendOtp.response = 'ERROR';
        setTimeout(() => {
            newState.controlState.sendOtp.time = moment().diff(start, 'milliseconds');
            newState.controlState.showForm = true;
            this.setState(newState);
            this.responseState = this.responsesInit;
        }, 5000);
    }

    buildsendOtpReq = (reqCardRes) => {
        const newState = {...this.state};
        newState.controlState.sendOtp.state = 'Enviado';
        this.setState(newState);

        const {issuerCardId} = reqCardRes;
        const sendOtp = {};
        sendOtp.issuerCardId = issuerCardId;
        sendOtp.wallet = this.state.wallet;
        this.otp = Math.floor(Math.random() * ((987689 + 1) - 123432) + 123432);
        sendOtp.otp = this.otp;

        return sendOtp;
    }

    buildsendOtpReqHeaders = () => {
        return {
            "Accept": "*/*",
            "transaccionId": this.correlationId
        };
    }

    validatesendOtpResponse = (req, res) => {
        return true;
    }

    handleVerifyOtp = async (e) => {
        e.preventDefault();
        const otp = this.input.current.value;

        const newState = {...this.state};
        newState.controlState.showOtp = false;
        this.setState(newState);

        if (otp === this.otp.toString()) {
            const notifyRes = await this.sendNotifyChangeRequest();
            console.log(notifyRes);
        } else {
            this.buildOtpError(moment());
        }
    }

    handleClose = () => {
        const newState = {...this.state};
        newState.controlState.sendOtp.state = 'Finalizado';
        newState.controlState.sendOtp.response = 'ERROR';
        //newState.controlState.sendOtp.time = moment().diff(start, 'milliseconds');
        newState.controlState.showForm = true;
        this.responseState = this.responsesInit;
        newState.controlState.showOtp = false;
        this.setState(newState);
    }

    handleReset = () => {
        const newState = {...this.state};
        newState.controlState = this.controlStateInit;
        newState.controlState.showForm = true;
        this.setState(newState);
    }

    render() {
        const {checkCard, reqCard, sendOtp, notify} = this.state.controlState;

        const checkCardColor = checkCard.state === 'No iniciado' ? 'secondary' :
            checkCard.state === 'Enviado' ? 'warning' :
                checkCard.response === 'OK' ? 'success' : 'danger';

        const reqCardColor = reqCard.state === 'No iniciado' ? 'secondary' :
            reqCard.state === 'Enviado' ? 'warning' :
                reqCard.response === 'OK' ? 'success' : 'danger';

        const sendOtpColor = sendOtp.state === 'No iniciado' ? 'secondary' :
            sendOtp.state === 'Enviado' ? 'warning' :
                sendOtp.response === 'OK' ? 'success' : 'danger';

        const notifyColor = notify.state === 'No iniciado' ? 'secondary' :
            notify.state === 'Enviado' ? 'warning' :
                notify.response === 'OK' ? 'success' : 'danger';

        return (
            <>
                <Modal
                    show={this.state.controlState.showOtp}
                    size="lg"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered
                >
                    <form>
                        <Modal.Header>
                            <Modal.Title id="contained-modal-title-vcenter">
                                INGRESAR OTP
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <p> Ingresa el OTP enviado al correo a continuación </p>
                            <input className="form-control" type="text" name="otp" id="otp" ref={this.input}/>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button type="submit" className="btn-primary"
                                    onClick={this.handleVerifyOtp}>Verificar</Button>
                            <Button className="btn-danger" onClick={this.handleClose}>Cancelar</Button>
                        </Modal.Footer>
                    </form>
                </Modal>
                {this.state.controlState.showForm ?
                    <div className="card col-md-6 col-sm-12 mx-auto mt-4 mb-4">
                        <div className="card-header bg-info text-white">
                            <h4 className="text-center">Información digitalización</h4>
                        </div>


                        <div className="card-body p-5">
                            <form onSubmit={this.handleSubmit}>
                                <div id="PaymentForm">
                                    <Cards
                                        cvc={this.state.cvc}
                                        expiry={this.state.expiryDate}
                                        focused={this.state.focus}
                                        name={this.state.cardHolder}
                                        number={this.state.cardNumber}
                                    />
                                    <div className="mt-4">
                                        <div className="form-group row mb-3">
                                            <label className="col-md-4 col-sm-12 col-form-label fw-bold">Numero de
                                                tarjeta: </label>
                                            <div className="col-md-8 col-sm-12 ">
                                                <input
                                                    className="form-control col-6"
                                                    type="tel"
                                                    name="cardNumber"
                                                    minLength="16"
                                                    maxLength="16"
                                                    required
                                                    placeholder="Numero tarjeta"
                                                    onChange={this.handleInputChange}
                                                    onFocus={this.handleInputFocus}
                                                />
                                            </div>
                                        </div>
                                        <div className="form-group row mb-3">
                                            <label className="col-md-4 col-sm-12 col-form-label fw-bold">Fecha de
                                                vencimiento: </label>
                                            <div className="col-md-8 col-sm-12">
                                                <input
                                                    className="form-control col-6"
                                                    type="tel"
                                                    name="expiryDate"
                                                    minLength="4"
                                                    maxLength="4"
                                                    required
                                                    placeholder="Fecha expiracion"
                                                    onChange={this.handleInputChange}
                                                    onFocus={this.handleInputFocus}
                                                />
                                            </div>
                                        </div>
                                        <div className="form-group row mb-3">
                                            <label className="col-md-4 col-sm-12 col-form-label fw-bold">Codigo de
                                                seguridad: </label>
                                            <div className="col-md-8 col-sm-12">
                                                <input
                                                    className="form-control col-6"
                                                    type="tel"
                                                    name="cvc"
                                                    minLength="3"
                                                    maxLength="4"
                                                    required
                                                    placeholder="Codigo seguridad"
                                                    onChange={this.handleInputChange}
                                                    onFocus={this.handleInputFocus}
                                                />
                                            </div>
                                        </div>
                                        <div className="form-group row mb-3">
                                            <label
                                                className="col-md-4 col-sm-12  col-form-label fw-bold">Propietario: </label>
                                            <div className="col-md-8 col-sm-12">
                                                <input
                                                    className="form-control col-6"
                                                    type="text"
                                                    name="cardHolder"
                                                    minLength="4"
                                                    maxLength="20"
                                                    required
                                                    placeholder="Propietario"
                                                    onChange={this.handleInputChange}
                                                    onFocus={this.handleInputFocus}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <h4>Información adicional</h4>
                                <div className="form-group row mb-3">
                                    <label className="col-md-4 col-sm-12  col-form-label fw-bold">Documento: </label>
                                    <div className="col-md-8 col-sm-12">
                                        <input
                                            className="form-control col-6"
                                            type="number"
                                            name="document"
                                            value={this.state.document}
                                            minLength="6"
                                            maxLength="13"
                                            required
                                            placeholder="Documento de identidad"
                                            onChange={this.handleInputChange}
                                        />
                                    </div>
                                </div>
                                <div className="form-group row mb-3">
                                    <label className="col-md-4 col-sm-12 col-form-label fw-bold">Billetera: </label>
                                    <div className="col-md-8 col-sm-12">
                                        <select
                                            className="form-select col-6"
                                            name="wallet"
                                            onChange={this.handleInputChange}
                                            value={this.state.wallet}
                                        >
                                            <option value="GGRN">GGRN</option>
                                            <option value="PAYPAL">Paypal</option>
                                            <option value="APPLE_PAY">ApplePay</option>
                                            <option value="SAMSUNG_PAY">SamsungPay</option>
                                            <option value="GARMIN_PAY">GarminPay</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="form-group row mb-3">
                                    <label className="col-md-4 col-sm-12 col-form-label fw-bold">Metodo de
                                        captura: </label>
                                    <div className="col-md-8 col-sm-12">
                                        <select
                                            className="form-select col-6"
                                            name="captureMethod"
                                            onChange={this.handleInputChange}
                                            value={this.state.captureMethod}
                                        >
                                            <option value="MANUAL">Manual</option>
                                            <option value="ARCHIVO">Archivo</option>
                                            <option value="CAMARA">Camara</option>
                                            <option value="APP">App</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="form-group row mb-3">
                                    <label className="col-md-4 col-sm-12 col-form-label fw-bold">Nivel de
                                        confianza: </label>
                                    <div className="col-md-8 col-sm-12">
                                        <select
                                            className="form-select col-6"
                                            name="levelOfTrust"
                                            onChange={this.handleInputChange}
                                            value={this.state.levelOfTrust}
                                        >
                                            <option value="VERDE">Verde</option>
                                            <option value="AMARILLO">Amarillo</option>
                                            <option value="ROJO">Rojo</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="form-group row mb-3">
                                    <label className="col-md-4 col-sm-12 col-form-label fw-bold">Puntaje
                                        seguridad: </label>
                                    <div className="col-md-8 col-sm-12">
                                        <select
                                            className="form-select col-6"
                                            name="securityScore"
                                            onChange={this.handleInputChange}
                                            value={this.state.securityScore}
                                            required
                                        >
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                        </select>
                                    </div>
                                </div>
                                <hr/>
                                <button className="btn btn-primary"
                                        type="submit">
                                    Tokenizar tarjeta
                                </button>
                            </form>
                        </div>
                    </div>
                    : null
                }
                <div className="card col-md-8 col-sm-12 mx-auto mt-4 mb-4">
                    <div className="card-header">
                        <h5>Estado de transacción</h5>
                    </div>
                    <div className="card-body">
                        <ul className="list-group">
                            <li className="row m-4">
                                <div className={`btn btn-${checkCardColor} col-md-3 col-sm-12`}>CheckCard</div>
                                <div className="col-md-3 col-sm-12"><p><b>Estado: </b>{checkCard.state}</p></div>
                                <div className="col-md-3 col-sm-12">
                                    {checkCard.state === 'No iniciado' ?
                                        null : checkCard.state === 'Enviado'
                                            ? <BeatLoader color="#009975" loading="true" css={override} size={10}/>
                                            : <span><b>Respuesta:</b> {checkCard.response}</span>}
                                </div>
                                <div className="col-md-3 col-sm-12">
                                    {checkCard.state === 'No iniciado' ?
                                        null : checkCard.state !== 'Finalizado' ?
                                            null : <span><b>Tiempo:</b> {checkCard.time} ms</span>}
                                </div>
                            </li>
                            <li className="row m-4">
                                <div className={`btn btn-${reqCardColor} col-md-3 col-sm-12`}>RequestCard</div>
                                <div className="col-md-3 col-sm-12"><p><b>Estado: </b>{reqCard.state}</p></div>
                                <div className="col-md-3 col-sm-12">
                                    {reqCard.state === 'No iniciado' ?
                                        null : reqCard.state === 'Enviado'
                                            ? <BeatLoader color="#009975" loading="true" css={override} size={10}/>
                                            : <span><b>Respuesta:</b> {reqCard.response}</span>}
                                </div>
                                <div className="col-md-3 col-sm-12">
                                    {reqCard.state === 'No iniciado' ?
                                        null : reqCard.state !== 'Finalizado' ?
                                            null : <span><b>Tiempo:</b> {reqCard.time} ms</span>}
                                </div>
                            </li>
                            {sendOtp.state === 'Inactivo' ? null :
                                <li className="row m-4">
                                    <div className={`btn btn-${sendOtpColor} col-md-3 col-sm-12`}>SendOtp</div>
                                    <div className="col-md-3 col-sm-12"><p><b>Estado: </b>{sendOtp.state}</p></div>
                                    <div className="col-md-3 col-sm-12">
                                        {sendOtp.state === 'No iniciado' ?
                                            null : sendOtp.state === 'Enviado'
                                                ? <BeatLoader color="#009975" loading="true" css={override} size={10}/>
                                                : <span><b>Respuesta:</b> {sendOtp.response}</span>}
                                    </div>
                                    <div className="col-md-3 col-sm-12">
                                        {sendOtp.state === 'No iniciado' ?
                                            null : sendOtp.state !== 'Finalizado' ?
                                                null : <span><b>Tiempo:</b> {sendOtp.time} ms</span>}
                                    </div>
                                </li>
                            }
                            <li className="row m-4">
                                <div className={`btn btn-${notifyColor} col-md-3 col-sm-12`}>NotifyChange</div>
                                <div className="col-md-3 col-sm-12"><p><b>Estado: </b>{notify.state}</p></div>
                                <div className="col-md-3 col-sm-12">
                                    {notify.state === 'No iniciado' ?
                                        null : notify.state === 'Enviado'
                                            ? <BeatLoader color="#009975" loading="true" css={override} size={10}/>
                                            : <span><b>Respuesta:</b> {notify.response}</span>}
                                </div>
                                <div className="col-md-3 col-sm-12">
                                    {notify.state === 'No iniciado' ?
                                        null : notify.state !== 'Finalizado' ?
                                            null : <span><b>Tiempo:</b> {notify.time} ms</span>}
                                </div>
                            </li>
                        </ul>
                        {this.state.controlState.showForm || notify.response !== 'OK' ? null :
                            <Button onClick={this.handleReset}>Tokenizar otra tarjeta</Button>}
                    </div>
                </div>
            </>

        );
    }
}