import PaymentForm from "./components/payment";
import 'react-credit-cards/es/styles-compiled.css';

function App() {
    return (
        <div className={"container-fluid mt-4"}>
            <PaymentForm/>
        </div>
    );
}

export default App;
